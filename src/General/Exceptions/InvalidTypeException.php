<?php  namespace Fenix440\Model\Type\General\Exceptions;

/**
 * Class InvalidTypeException
 *
 * Throws an exception if type is invalid
 *
 * @package      Fenix440\Model\Type\General\Exceptions
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 */
class InvalidTypeException extends \InvalidArgumentException{

}