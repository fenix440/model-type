<?php  namespace Fenix440\Model\Type\General\Traits;
use Aedart\Validate\AnythingValidator;
use Fenix440\Model\Type\General\Exceptions\InvalidTypeException;

/**
 * Trait TypeTrait
 * A component should use this trait if type
 * property is required and the type value for this
 * property is unknown or mixed
 * @see TypeAware
 *
 * @package      Fenix440\Model\Type\General\Traits
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 */
trait TypeTrait {

    /**
     * Type for given component
     *
     * @var null|mixed
     */
    protected $type=null;

    /**
     * Set type for given component
     * @param mixed $type Type for given component
     * @return void
     * @throws InvalidTypeException If type is invalid
     */
    public function setType($type){
        if(!$this->isTypeValid($type))
            throw new InvalidTypeException(sprintf('Type %s is invalid',$type));
        $this->type=$type;
    }

    /**
     * Validates if type is valid
     *
     * @param $type Type for given component
     * @return bool true/false
     */
    public function isTypeValid($type){
        return AnythingValidator::isValid($type);
    }

    /**
     * Get type
     * @return mixed|null
     */
    public function getType(){
        if(!$this->hasType() && $this->hasDefaultType())
            $this->setType($this->getDefaultType());
        return $this->type;
    }

    /**
     * Get Default type
     *
     * @return mixed|null
     */
    public function getDefaultType(){
        return null;
    }

    /**
     * Checks if default type is set
     *
     * @return bool true/false
     */
    public function hasDefaultType(){
        return (!is_null($this->getDefaultType()))? true:false;
    }

    /**
     * Checks if type is set
     *
     * @return bool true/false
     */
    public function hasType(){
        return (!is_null($this->type))? true:false;
    }
}