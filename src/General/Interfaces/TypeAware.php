<?php namespace Fenix440\Model\Type\General\Interfaces;
use Fenix440\Model\Type\General\Exceptions\InvalidTypeException;

/**
 * Interface TypeAware
 *
 * A component must be aware of type
 *
 * @package Fenix440\Model\Type\General\Interfaces
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 */
interface TypeAware
{

    /**
     * Set type for given component
     * @param mixed $type Type for given component
     * @return void
     * @throws InvalidTypeException If type is invalid
     */
    public function setType($type);

    /**
     * Validates if type is valid
     *
     * @param $type Type for given component
     * @return bool true/false
     */
    public function isTypeValid($type);

    /**
     * Get type
     * @return mixed|null
     */
    public function getType();

    /**
     * Get Default type
     *
     * @return mixed|null
     */
    public function getDefaultType();

    /**
     * Checks if default type is set
     *
     * @return bool true/false
     */
    public function hasDefaultType();

    /**
     * Checks if type is set
     *
     * @return bool true/false
     */
    public function hasType();

}

 