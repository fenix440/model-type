<?php  namespace Fenix440\Model\Type\Integer\Traits;
use Aedart\Validate\Number\IntegerValidator;
use Fenix440\Model\Type\General\Traits\TypeTrait as GeneralTypeTrait;

/**
 * Trait TypeTrait
 *
 * A component should use this trait if type property
 * is required and the type value is integer
 *
 * @see TypeAware
 *
 * @package      Fenix440\Model\Type\Integer\Traits
 * @author      Bartlomiej Szala <fenix440@gmail.com>
*/
trait TypeTrait {

    use GeneralTypeTrait;
    /**
     * Validates if type is valid
     *
     * @param $type Type for given component
     * @return bool true/false
     */
    public function isTypeValid($type){
        return IntegerValidator::isValid($type);
    }
}