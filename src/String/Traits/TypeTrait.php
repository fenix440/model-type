<?php  namespace Fenix440\Model\Type\String\Traits;
use Aedart\Validate\StringValidator;
use Fenix440\Model\Type\General\Traits\TypeTrait as GeneralTypeTrait;

/**
 * Trait TypeTrait
 *
 * Component should use this trait if type
 * property is required and the type value is string.
 * @see TypeAware
 *
 * @package      Fenix440\Model\Type\String\Traits
 * @author      Bartlomiej Szala <fenix440@gmail.com>
*/
trait TypeTrait {

    use GeneralTypeTrait;

    /**
     * Validates if type is valid
     *
     * @param $type Type for given component
     * @return bool true/false
     */
    public function isTypeValid($type){
        return StringValidator::isValid($type);
    }


}