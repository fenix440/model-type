## Model-Type ##

Getter and Setter package for some kind of model Type.

This package is part of a project `Aedart\Model`, visit [https://bitbucket.org/aedart/model](https://bitbucket.org/aedart/model) to learn more about it.

Official sub-package website ([https://bitbucket.org/fenix440/model-type](https://bitbucket.org/fenix440/model-type))

## Contents ##

[TOC]

## When to use this ##

When your component(s) need to be aware of some kind of a Type

## How to install ##

```
#!console

composer require fenix440/model-type versionNumber
```

This package uses [composer](https://getcomposer.org/). If you do not know what that is or how it works, I recommend that you read a little about, before attempting to use this package.

## Quick start ##


Provided that you have an interface, e.g. for a product, you can extend the Type aware interface;

```
#!php
<?php
use Fenix440\Model\Type\General\Interfaces\TypeAware;

interface IProduct extends TypeAware{

    // ... Remaining interface implementation not shown

}
```

In your class implementation, you simple use one of the name traits.

```
#!php
<?php
use Fenix440\Model\Type\General\Traits\TypeTrait;

class MyProduct implements IProduct {
   
     use TypeTrait;
   
     // ... Remaining implementation not shown...

}
```

## License ##

[BSD-3-Clause](http://spdx.org/licenses/BSD-3-Clause), Read the LICENSE file included in this package