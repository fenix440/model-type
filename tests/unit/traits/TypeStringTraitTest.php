<?php
use Fenix440\Model\Type\String\Traits\TypeTrait;
use Fenix440\Model\Type\General\Interfaces\TypeAware;

/**
 * Class TypeTraitTest
 *
 * @coversDefaultClass Fenix440\Model\Type\String\Traits\TypeTrait
 *  @author Bartlomiej Szala <fenix440@gmail.com>
 */
class TypeStringTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /************************************************************************
         * Data "providers"
         ***********************************************************************/

        /**
         * Get the trait mock
         *
         * @return PHPUnit_Framework_MockObject_MockObject|Fenix440\Model\Type\General\Interfaces\TypeAware
         */
        protected function getTraitMock() {
            return $this->getMockForTrait('Fenix440\Model\Type\String\Traits\TypeTrait');
        }

        /************************************************************************
         * Actual tests
         ***********************************************************************/


    /**
     * @test
     * @covers  ::setType
     * @covers  ::isTypeValid
     * @covers  ::getType
     * @covers  ::getDefaultType
     * @covers  ::hasDefaultType
     * @covers  ::hasType
     *
     */
    public function setAndGetType()
    {
        $trait = $this->getTraitMock();
        $type = "new";
        $trait->setType($type);

        $this->assertSame($type,$trait->getType(),'Type is invalid');
    }

    /**
     * @test
     * @covers  ::setType
     * @covers  ::isTypeValid
     * @expectedException \Fenix440\Model\Type\General\Exceptions\InvalidTypeException
     */
    public function setInvalidType()
    {
        $trait = $this->getTraitMock();
        $type = 1;

        $trait->setType($type);
    }

    /**
     * @test
     * @covers  ::setType
     * @covers  ::isTypeValid
     * @expectedException \Fenix440\Model\Type\General\Exceptions\InvalidTypeException
     */
    public function setInvalidFloatType()
    {
        $trait = $this->getTraitMock();
        $type = 100.23;

        $trait->setType($type);
    }
}