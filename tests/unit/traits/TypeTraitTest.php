<?php
use Fenix440\Model\Type\General\Traits\TypeTrait;
use Fenix440\Model\Type\General\Interfaces\TypeAware;

/**
 * Class TypeTraitTest
 *
 * @coversDefaultClass Fenix440\Model\Type\General\Traits\TypeTrait
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 */
class TypeTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /************************************************************************
     * Data "providers"
     ***********************************************************************/

    /**
     * Get the trait mock
     *
     * @return PHPUnit_Framework_MockObject_MockObject|Fenix440\Model\Type\General\Interfaces\TypeAware
     */
    protected function getTraitMock()
    {
        return $this->getMockForTrait('Fenix440\Model\Type\General\Traits\TypeTrait');
    }

    /************************************************************************
     * Actual tests
     ***********************************************************************/


    /**
     * @test
     * @covers  ::setType
     * @covers  ::isTypeValid
     * @covers  ::getType
     * @covers  ::getDefaultType
     * @covers  ::hasDefaultType
     * @covers  ::hasType
     *
    */
    public function setAndGetIntegerType()
    {
        $trait = $this->getTraitMock();
        $type = 1;
        $trait->setType($type);

        $this->assertSame($type,$trait->getType(),'Type is invalid for integer');
    }

    /**
     * @test
     * @covers  ::setType
     * @covers  ::isTypeValid
     * @covers  ::getType
     * @covers  ::getDefaultType
     * @covers  ::hasDefaultType
     * @covers  ::hasType
     *
     */
    public function setAndGetFloatType()
    {
        $trait = $this->getTraitMock();
        $type = 1232.00;
        $trait->setType($type);

        $this->assertSame($type,$trait->getType(),'Type is invalid for float');
    }

    /**
     * @test
     * @covers  ::setType
     * @covers  ::isTypeValid
     * @covers  ::getType
     * @covers  ::getDefaultType
     * @covers  ::hasDefaultType
     * @covers  ::hasType
     *
     */
    public function setAndGetStringType()
    {
        $trait = $this->getTraitMock();
        $type = "my type";
        $trait->setType($type);

        $this->assertSame($type,$trait->getType(),'Type is invalid for a string');
    }

    /**
     * @test
     * @covers  ::setType
     * @covers  ::getType
     * @covers  ::getDefaultType
     * @covers  ::hasDefaultType
     * @covers  ::hasType
     * @covers  ::isTypeValid
     *
    */
    public function testDefaultType()
    {
        $trait = $this->getTraitMock();
        $type=null;
        $trait->setType($type);

        $this->assertNull($trait->getDefaultType(),'Default type is not null!');
        $this->assertSame($type,$trait->getType(),'Default type is not null!');
    }



}